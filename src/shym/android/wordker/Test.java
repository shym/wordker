package shym.android.wordker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Test extends Activity{
	
	private String[] ang = null;
	private String[] pol = null;
	private String setName = null;
	private static final int SELECT_SET = 1;
	
	private int correct = 0;
	private int incorrect = 0;
	private int counter = 0;
	private boolean isSetListener = false;
	private boolean isSetLearningView = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.setTitle(R.string.pickSet);
	}
	
	private void fillData(){
		if(setName==null){
			noSetAlert();
		}
		else{
			if(!isSetLearningView){
				setContentView(R.layout.test);
				this.setTitle(getString(R.string.testing_title) +" "+ setName);
				isSetLearningView = true;
			}			
						
			TextView tv = (TextView) this.findViewById(R.id.slowko);
			tv.setText(pol[counter]);
			TextView Num = (TextView) findViewById(R.id.correctNum);
			Num.setText(correct+"");
			
			Num = (TextView) findViewById(R.id.incorrectNum);
			Num.setText(incorrect+"");
			
			Num = (TextView) findViewById(R.id.leftNum);
			Num.setText((pol.length - counter-1)+"");
			
			
			
			EditText et = (EditText) findViewById(R.id.wordToCheck);
			et.setText("");
			setListenersForButtons();
		}
	}
	
	private void checkWord(){
		counter++;
		EditText et = (EditText) findViewById(R.id.wordToCheck);
		String wordToCheck = et.getText().toString();
		et.clearFocus();
		if(wordToCheck.equalsIgnoreCase(ang[counter-1])){
			correct++;
		}
		else{
			incorrect++;
		}
		if(counter>=pol.length){
			TextView Num = (TextView) findViewById(R.id.correctNum);
			Num.setText(correct+"");
			
			Num = (TextView) findViewById(R.id.incorrectNum);
			Num.setText(incorrect+"");
			
			Num = (TextView) findViewById(R.id.leftNum);
			Num.setText((pol.length - counter)+"");
			
			double wynik = ((double)correct/(double)pol.length)*100.0;
			
			AlertDialog.Builder builder = new AlertDialog.Builder(Test.this);
			builder.setMessage(getString(R.string.setFinished)+" " + (int)wynik + "%")
				   .setCancelable(false)
				   .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						finish();							
					}
				});				
			AlertDialog alert = builder.create();
			alert.show();
		}else
			fillData();
	}
	
	private void selectSet(){
		
    	Intent i = new Intent(this, ChooseSet.class);
    	startActivityForResult(i, SELECT_SET);
		
	}
	
	private void noSetAlert(){

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.before_testing))
		       .setCancelable(false)
		       .setNegativeButton("Cofnij", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			})
		       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		        	   selectSet();			        	   
		        	   dialog.dismiss();
		           }
		       });
		AlertDialog alert = builder.create();
		alert.show();
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(setName == null){
			noSetAlert();
		}
		else{
			fillData();
			if(setName!=null)
				setListenersForButtons();
		}
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putStringArray("pol", pol);
		outState.putStringArray("ang", ang);
		outState.putInt("counter", counter);
		outState.putInt("correct", correct);
		outState.putInt("incorrect", incorrect);
		outState.putString("setName", setName);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState){
		pol = savedInstanceState.getStringArray("pol");
		ang = savedInstanceState.getStringArray("ang");
		counter = savedInstanceState.getInt("counter");
		correct = savedInstanceState.getInt("correct");
		incorrect = savedInstanceState.getInt("incorrect");
		setName = savedInstanceState.getString("setName");
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	private void setListenersForButtons(){
		
		if(!isSetListener){
			Button checkButton = (Button) findViewById(R.id.checkButton);
			checkButton.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) {
					checkWord();
				}
				
				
			});
		}
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode){
		case SELECT_SET:
			if(data!=null){
				ang = data.getStringArrayExtra("ang");
				pol = data.getStringArrayExtra("pol");
				setName = data.getStringExtra("name");
				Toast.makeText(this, "Wybrales zestaw "+setName, Toast.LENGTH_SHORT).show();
			}
			fillData();
		}
		
	}

	
	
	

}

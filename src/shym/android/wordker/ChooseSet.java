package shym.android.wordker;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class ChooseSet extends ListActivity{

	private SetsDbAdapter mDbHelper;
	private String[] ang;
	private String[] pol;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sets_list);
		setTitle(R.string.chooseTitle);
		
		mDbHelper = new SetsDbAdapter(this);
		mDbHelper.open();
		fillData();
		registerForContextMenu(getListView());
		mDbHelper.close();
	}
	
	private void fillData(){
		
		Cursor setsCursor = mDbHelper.fetchAllSets();
		startManagingCursor(setsCursor);
		
		// Create an array to specify the fields we want to display in the list (only TITLE)
        String[] from = new String[]{SetsDbAdapter.KEY_NAME};

        // and an array of the fields we want to bind those fields to (in this case just text1)
        int[] to = new int[]{R.id.text1};
		
		SimpleCursorAdapter notes = 
	            new SimpleCursorAdapter(this, R.layout.sets_row, setsCursor, from, to);
	        setListAdapter(notes);
		
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		Cursor c = (Cursor) getListView().getItemAtPosition(position);
		final String nazwaZestawu = c.getString(c.getColumnIndexOrThrow(SetsDbAdapter.KEY_NAME));
		final Long sId = Long.parseLong(c.getString(c.getColumnIndexOrThrow(SetsDbAdapter.KEY_ROWID)));
    	builder.setMessage("Czy na pewno chcesz wybra�:\n" + nazwaZestawu)
    	       .setCancelable(false)
    	       .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	        	   selectSet(sId);
    	           }
    	       })
    	       .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	                dialog.cancel();
    	           }
    	       });
    	AlertDialog alert = builder.create();
    	alert.show();
		
	}
	
	private void selectSet(Long id){
		mDbHelper.open();
		Cursor c = mDbHelper.fetchSet(id);
		this.startManagingCursor(c);
		ang = c.getString(c.getColumnIndexOrThrow(SetsDbAdapter.KEY_ANG)).split("~");
		pol = c.getString(c.getColumnIndexOrThrow(SetsDbAdapter.KEY_POL)).split("~");
		String name = c.getString(c.getColumnIndexOrThrow(SetsDbAdapter.KEY_NAME));
		c.close();
		
		mDbHelper.close();
		
		Intent intent = getIntent();
		intent.putExtra("ang", ang);
		intent.putExtra("pol", pol);
		intent.putExtra("name", name);
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}
	
	

}

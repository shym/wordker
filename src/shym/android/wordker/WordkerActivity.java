package shym.android.wordker;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class WordkerActivity extends Activity {
	
	public static final int SELECT_SETS = 1;
	public static final int TEST = 2;
	public static ProgressDialog dialog;
	private SetsDbAdapter mDbHelper;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setListenersForButtons();
        refreshNumber();
        checkPreferences();
    }
    
    private void selectSets(){
    	Intent i = new Intent(this, SetSelect.class);
    	startActivityForResult(i, SELECT_SETS);
    }
    
    private void startLearning(){
    	Intent i = new Intent(this, Test.class);
    	startActivityForResult(i, TEST);
    }
    
    private void checkPreferences(){
    	
    }
    
    private void setListenersForButtons(){
    	
    	Button downloadSetsButton = (Button) findViewById(R.id.button1);
        Button learnButton = (Button) findViewById(R.id.button2);
        
        learnButton.setOnClickListener(new OnClickListener(){
        	
        	@Override
        	public void onClick(View v){
        		startLearning();
        	}
        });
        downloadSetsButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				dialog = ProgressDialog.show(WordkerActivity.this, "", getString(R.string.wait_downloading));
				selectSets();
			}
        	
        	
        });
        
    	
    }
    
    private void refreshNumber(){
    	
    	mDbHelper = new SetsDbAdapter(this);
    	mDbHelper.open();
    	
    	Cursor c = mDbHelper.fetchAllSets();
    	
    	long n = c.getCount();
    	c.close();
    	
    	TextView tv = (TextView) this.findViewById(R.id.downloadedSets);
    	tv.setText(getString(R.string.downloaded_sets)+" "+n);
    	
    	mDbHelper.close();
    	
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode){
		
		case SELECT_SETS:
			refreshNumber();
		case TEST:
			if(resultCode == RESULT_CANCELED){
				
			}
		
		}
	}
    
}
package shym.android.wordker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SetsDbAdapter {
	
	public static final String KEY_ANG = "ang";
	public static final String KEY_POL = "pol";
	public static final String KEY_NAME = "nazwa";
	public static final String KEY_ROWID = "_id";
	
	private static final String TAG = "SetsDbAdapter";
	private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;
	
	/**
	 * Stejtment tworzenia bazy danych :d
	 **/
	
	private static final String DATABASE_CREATE = 
			"create table sets (_id integer primary key autoincrement, "
					+ "nazwa text not null, ang text not null, pol text not null);";
	
	private static final String DATABASE_NAME = "data";
    private static final String DATABASE_TABLE = "sets";
    private static final int DATABASE_VERSION = 2;
    
    private final Context mCtx;
    
    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS notes");
            onCreate(db);
        }
    }
    
    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     * 
     * @param ctx the Context within which to work
     */
    public SetsDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }
    
    /**
     * Open the notes database. If it cannot be opened, try to create a new
     * instance of the database. If it cannot be created, throw an exception to
     * signal the failure
     * 
     * @return this (self reference, allowing this to be chained in an
     *         initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    public SetsDbAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }
    
    /**
     * Create a new note using the title and body provided. If the note is
     * successfully created return the new rowId for that note, otherwise return
     * a -1 to indicate failure.
     * 
     * @param title the title of the note
     * @param body the body of the note
     * @return rowId or -1 if failed
     */
    public long addSet(String nazwa, String ang, String pol) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_NAME, nazwa);
        initialValues.put(KEY_ANG, ang);
        initialValues.put(KEY_POL, pol);

        return mDb.insert(DATABASE_TABLE, null, initialValues);
    }

    /**
     * Delete the note with the given rowId
     * 
     * @param rowId id of note to delete
     * @return true if deleted, false otherwise
     */
    public boolean deleteSet(String nazwa) {

        return mDb.delete(DATABASE_TABLE, KEY_NAME + "=" + nazwa, null) > 0;
    }
    
    /**
     * Return a Cursor over the list of all notes in the database
     * 
     * @return Cursor over all notes
     */
    public Cursor fetchAllSets() {

        return mDb.query(DATABASE_TABLE, null, null, null, null, null, null);
    }
    
    public Cursor fetchSet(Long id) throws SQLException {

        Cursor mCursor =

            mDb.query(true, DATABASE_TABLE, new String[] {KEY_ROWID,
                    KEY_NAME, KEY_ANG, KEY_POL}, KEY_ROWID + "=" + id, null,
                    null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }

}

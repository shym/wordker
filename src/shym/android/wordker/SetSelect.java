package shym.android.wordker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class SetSelect extends ListActivity{
	
	private HashMap<String, String> zestawy = new HashMap<String, String>();
	private String[] from;
	private SetsDbAdapter mDbHelper;
	private String nazwaZestawu;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		WordkerActivity.dialog.dismiss();
		setTitle(R.string.select_title);		
		setContentView(R.layout.sets_list);
		populateList();
	}
	
	private void populateList(){
		try {
			downloadSets();
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
		from = new String[zestawy.size()];
		int i=0;
		for(String key : zestawy.keySet()){
			from[i] = zestawy.get(key);
			i++;
		}
		setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item, from));
	}
	
	@Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
    	super.onListItemClick(l, v, position, id);
    	
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	nazwaZestawu = (String) getListView().getItemAtPosition(position);
    	builder.setMessage("Czy na pewno chcesz pobra�:\n" + nazwaZestawu)
    	       .setCancelable(false)
    	       .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	                for(String key : zestawy.keySet()){
    	                	if(zestawy.get(key).equalsIgnoreCase(nazwaZestawu))
								try {
									insertSet(key);
									break;
								} catch (IOException e) {
									break;
								}
    	                }
    	           }
    	       })
    	       .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	                dialog.cancel();
    	           }
    	       });
    	AlertDialog alert = builder.create();
    	alert.show();
    }
	
	private void downloadSets() throws IOException{
		
		URL wordki = new URL("http://wordki.pl");
		URLConnection uc = wordki.openConnection();
		BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream()), 8*1024);
		
		while(!br.readLine().contains("id=\"PoLewej\""));
		String line = "";
		
		while(!(line = br.readLine()).contains("<!-- nowePoLewej -->")){
			String[] ln = line.split(">");
			String nazwa = wytnijNazwe(ln[1]);
			String nazwapl = new String(nazwa.getBytes(), "utf-8");
			
			nazwapl = nazwapl.replaceAll("&#62;", ">");
			nazwapl = nazwapl.replaceAll("&#34;", "\"");
			nazwapl = nazwapl.replaceAll("&amp;", "&");
			if(nazwapl.length()>40)
				nazwapl = nazwapl.substring(0, 5);
			
			zestawy.put(wytnijLink(ln[0]), nazwapl);
		}
		
	}
	
	private void insertSet(String key) throws IOException{
		
		URL wordka = new URL("http://wordki.pl/" + key);
		URLConnection uc = wordka.openConnection();
		BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream()), 8*1024);
		
		while(!br.readLine().contains("id=\"Slowa\""));
		String line = br.readLine();
		String[] ln = line.split("</td><td>");
		
		String ang = ln[0].substring(8);
		String pol = ln[1].substring(0, ln[1].length()-10);
		
		while(!(line = br.readLine()).contains("</table>")){
			
			ln = line.split("</td><td>");
			
			ang += "~" + ln[0].substring(8);
			pol += "~" + ln[1].substring(0, ln[1].length()-10);
			
		}
		
		mDbHelper = new SetsDbAdapter(this);
		mDbHelper.open();
		mDbHelper.addSet(zestawy.get(key), ang, pol);
		mDbHelper.close();
		
	}
	
	private String wytnijLink(String tx){
		String result = "";
		
		for(int i=9;tx.toCharArray()[i]!="\"".toCharArray()[0];i++){
			result += tx.toCharArray()[i];
		}
		
		
		return result;
	}
	
	private String wytnijNazwe(String tx){
		String result ="";
		
		result = tx.replaceFirst("</a", "");
		
		return result;
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}
	

}
